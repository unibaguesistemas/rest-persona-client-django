import requests
import json

from .persona import Persona


class Controller:
    URL = 'http://localhost:9000/personas'

    def __init__(self):
        pass

    @staticmethod
    def buscar(i):
        response = requests.get(Controller.URL + "/" + str(i))
        content = response.content
        if content:
            p = json.loads(content)
            persona = Persona(p['nombre'], p['profesion'], p['fecha'])
            persona.set_id(p['id'])
            return persona
        else:
            return None

    @staticmethod
    def listar():
        lista_personas = []
        response = requests.get(Controller.URL)
        content = response.content
        data = json.loads(content)
        for p in data:
            persona = Persona(p['nombre'], p['profesion'], p['fecha'])
            persona.set_id(p['id'])
            lista_personas.append(persona)
        return lista_personas

    @staticmethod
    def crear(persona):
        headers = {'content-type': 'application/json'}
        response = requests.post(Controller.URL, data=persona.comoJson(), headers=headers)
        return response.content

    @staticmethod
    def eliminarPersona(i):
        requests.delete(Controller.URL + "/" + str(i))

    @staticmethod
    def actualizar(i, persona):
        headers = {'content-type': 'application/json'}
        persona.set_id(1)
        resp = requests.put(Controller.URL + "/" + str(i), data=persona.comoJson(), headers=headers)
        if resp.status_code != 204:
            return 'Hubo un error actualizando'
