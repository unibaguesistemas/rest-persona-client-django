"""
Definition of views.
"""

from django.shortcuts import render
from django.http import HttpRequest
from datetime import datetime
from .controller import Controller
from .forms import *
from .persona import Persona
from django.contrib import messages


def formBuscar(request):
    if request.method == 'POST':
        form = BuscarForm(request.POST)
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return buscarPersona(request, form.cleaned_data['idBuscar'])
    else:
        form = BuscarForm()
    return render(
        request,
        'app/formBuscar.html',
        {
            'title': 'Buscar Persona',
            'year': datetime.now().year,
            'form': form
        }
    )


def home(request):
    controller = Controller()
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title': 'Lista de Personas',
            'message': 'Your application description page.',
            'year': datetime.now().year,
            'lista': controller.listar(),
        }
    )


def eliminarPersona(request):
    if request.method == 'POST':
        form = EliminarForm(request.POST)
        if form.is_valid():
            controller = Controller()
            error = controller.eliminarPersona(form.cleaned_data['idEliminar'])
            if error:
                messages.info(request, 'No se elimino: ' + error)
            else:
                messages.info(request, 'Se elimino')
            return home(request)
        else:
            form = EliminarForm()
    else:
        form = EliminarForm()
    return render(
        request,
        'app/formEliminar.html',
        {
            'title': 'Eliminar Persona',
            'year': datetime.now().year,
            'form': form
        }
    )


def buscarPersona(request, id):
    """Renders the persona page."""
    assert isinstance(request, HttpRequest)
    controller = Controller()
    persona = controller.buscar(id)
    if persona:
        form = PersonaForm(initial=persona.comoArreglo())
        return render(
            request,
            'app/agregarPersona.html',
            {
                'title': 'Persona numero ' + str(id),
                'year': datetime.now().year,
                'form': form,
                'ruta': '/personas/editar',
                'idPast': id,
                'id': persona.id,
            }
        )
    else:
        messages.info(request, 'No se encontro una persona con el id ' + str(id))
        return home(request)


def crearPersona(request, data):
    controller = Controller()
    persona = Persona(data['nombre'], data['profesion'], data['fecha'])
    error = controller.crear(persona)
    if error:
        messages.info(request, 'No se creo: ' + error)
    else:
        messages.info(request, 'Se creo')
    return home(request)


def editarPersona(request):
    assert isinstance(request, HttpRequest)
    if request.method == 'POST':
        controller = Controller()
        form = PersonaForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            persona = Persona(data['nombre'], data['profesion'], data['fecha'])
            error = controller.actualizar(request.POST.get("idPast", ""), persona)
            if error:
                messages.info(request, 'No se actualizo: '+error)
            else:
                messages.info(request, 'Se actualizo')
        else:
            return render(
                request,
                'app/agregarPersona.html',
                {
                    'title': 'Agregar una Persona',
                    'year': datetime.now().year,
                    'form': form,
                    'idPast': 0
                }
            )

    return home(request)


def agregarPersona(request):
    assert isinstance(request, HttpRequest)
    if request.method == 'POST':
        form = PersonaForm(request.POST)
        if form.is_valid():
            return crearPersona(request, form.cleaned_data)
    else:
        form = PersonaForm()

    return render(
        request,
        'app/agregarPersona.html',
        {
            'title': 'Agregar una Persona',
            'year': datetime.now().year,
            'form': form,
            'idPast': 0
        }
    )
