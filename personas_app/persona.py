import json
from datetime import date, datetime


class Persona:
    id = None
    nombre = None
    profesion = None
    fecha = None

    def set_id(self, id):
        self.id = id

    def __init__(self, nombre, profesion, fecha):
        self.nombre = nombre
        self.profesion = profesion
        self.fecha = datetime.strptime(str(fecha), "%Y-%m-%d")

    def comoArreglo(self):
        return {
            'nombre': self.nombre,
            'profesion': self.profesion,
            'fecha': self.fecha
        }

    def comoJson(self):
        data = {
                'id': self.id,
                'nombre': self.nombre,
                'profesion': self.profesion,
                'fecha': str(self.fecha)
                }
        return json.dumps(data)
